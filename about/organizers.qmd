---
pagetitle: "Committee members"
---

::: grid
::: {#organizers_banner .g-col-12 .title}
# Committee Members
:::

::: {.g-col-6 .text_block}
### Organizing Committee

-   John Doe 1 *(ABC University)*
-   John Doe 2 *(XYZ University)*

### Program Committee

-   John Doe 1 *(ABC University)*
-   John Doe 2 *(XYZ University)*
:::

::: g-col-6
![](/img/user-conf.png){.img_block width="100%"}
:::
:::
