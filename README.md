# useR! 2024 website (useR2024.r-project.org)

This website is based on [satRdays Quarto template](https://github.com/satRdays/quarto-satrdays-template).

It is deployed using Netlify: see the pinned message on the #website channel of the organizers' Slack to find the Netlify URL (to use in place of `sitename.netlify.app` in the examples below).

 - Commits to main will be deployed to the production site (will be useR2024.r-project.org when set up).
 - Commits to branch `example` can be previewed at `example--sitename.netlify.app`. 
 - Merge request #42 will deploy to `deploy-preview-42--sitename.netlify.app`. 
 - Any change to the GitLab repo will trigger a deployment and there is usually no need to render locally (it does not hurt if you do as the result is not version controlled).
    - Netlify can only deploy markdown so if you use an executable cells in the `.qmd` then you will need to render locally first. However, this should be avoided to keep things simple.
    

